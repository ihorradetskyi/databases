<?php

namespace lessons\connections\database;

interface setCredentials
{
    public function setLogin($login);
    public function setPass($pass);
}

interface setServer
{
    public function setHost($host);
    public function setPort($port);
    
}

interface setDatabase
{
    public function setDatabase($database);
}

interface conncection
{
    public function connect($array);
    public function disconncect($connection);
}

interface query{
    public function getQuery($queryStr);
    public function printQuery($result);
}
