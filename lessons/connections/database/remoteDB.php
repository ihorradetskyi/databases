<?php

namespace lessons\connections\database;

include('setCredentials.php');

class remoteDB implements setServer, setCredentials, setDatabase{
    
    public $remoteHost;
    public $port;
    public $login;
    public $pass;
    public $database;

    public $numberOfRows;
    
    public function __construct($array = array()) {
      $this->remoteHost = $array['host'];
      $this->port       = $array['port'];
      $this->login      = $array['login'];
      $this->pass       = $array['pass'];
      $this->database   = $array['database'];
    }
    
    public function setHost($host)
    {
        $this->remoteHost = $host;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function setPass($pass)
    {
        $this->pass = $pass;
    }
    
    public function setDatabase($database)
    {
        $this->database = $database;
    }
    
    public function printQuery($result)
    {
        $rows_count = 0;
	    $result = pg_fetch_all($result);
	
        foreach ($result as $r)
        {
	        $r = implode(" ", $r);
            $rows_count++;
            echo $r;
            echo "\n";
        }
        $this->numberOfRows = $rows_count;
        echo "Number of printed rows: ".$rows_count."\n";
    }

    public function resultQueryLines($result)
    {
        $rows_count = 0;
        $result = pg_fetch_all($result);
        $linedResult = array();
	
        foreach ($result as $r)
        {
	        $r = implode(" ", $r);
            $rows_count++;
            array_push($linedResult, $r."\n");
            //array_push($linedResult, $r);
        }
        $this->numberOfRows = $rows_count;
        return $linedResult;
    }
    
}

class mysqlDb extends remoteDB implements conncection, query{
    
    private $connection;
    
    public function __construct($array = array())
    {
        parent::__construct($array);
        $this->connection = connect($array);
    }
        
    public function disconncect($connection)
    {
        mysqli_close($connection);
        
    }

    public function connect($array)
    {
        return mysqli_connect(parent::$host, parent::$login, parent::$pass, parent::$database, parent::$port)
        or die ('Could not connect to the database server' . mysqli_connect_error());
    }
    

    public function getQuery($queryStr)
    {
        if($this->connection != null){
            printQuery(mysqli_query($this->connection, $queryStr));
            return 
            disconncect($this->connection);
        }else{
            //
        }
    }
    
}

class postDb extends remoteDB implements conncection, query{
    
    public $connection;
    
    public function __construct($array = array())
    {
        parent::__construct($array);
        //$this->connection = connect($array);
    }
    
	public function connect($array = array())
    {
        $this->connection=pg_connect("host=".$this->remoteHost." port=".$this->port." dbname=".$this->database."
        user=".$this->login." password=".$this->pass);
        echo "connected";
    }


    public function disconncect($connection)
    {
        pg_close($this->connection);
	    echo "disconnected";
    }
    
    
    
    public function getQuery($queryStr)
    {
        if($this->connection != null){
            return $this->resultQueryLines(pg_query($this->connection, $queryStr));
          //  disconncect($this->connection);
        }else{
            //
        }
    }

}

class Paginator {

    private $dataBase;
    private $rows = array();
    private $rowsOnPage = 20;
 
    public function __construct($array = array()){
        $this->dataBase = new postDb($array);
        $this->dataBase->connect();
        $this->rows = $this->dataBase->getQuery("SELECT * FROM public.actor ORDER BY actor_id ASC");
        $this->numberOfRows=$this->dataBase->numberOfRows;
        $this->dataBase->disconncect($this->dataBase->connection);
        //$this->rows = explode("\n", $this->rows);
    }

    public function printPage($page){
        if($page != 1){
            $last_line= $this->rowsOnPage*$page;
        } else {
            $last_line = $this->rowsOnPage;
        }
        $first_line = $last_line-$this->rowsOnPage;
        echo "\n";

        for($i = $first_line; $i<$last_line; $i++){
            echo $this->rows[$i];
        }
    }



}

	$con = array("host"=>"localhost", "port"=>"5432", "login"=>"postgres", "pass"=>"postgres", "database"=>"test");
    $db = new Paginator($con);
   // $db->connect();

///	$db->getQuery("SELECT * FROM public.actor ORDER BY actor_id ASC");

    //$db->disconncect($db->connection);
    
    $db->printPage(1);
    $db->printPage(2);
    $db->printPage(3);
    $db->printPage(4);
    $db->printPage(5);

